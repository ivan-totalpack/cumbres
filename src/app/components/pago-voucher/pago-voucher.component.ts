import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pago-voucher',
  templateUrl: './pago-voucher.component.html',
  styleUrls: ['./pago-voucher.component.css']
})
export class PagoVoucherComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
    //this.redirecthome();
  }

  redirecthome(){
    setTimeout(() => {
      this.router.navigate(['/']);
    }, 10000);
  }

}
