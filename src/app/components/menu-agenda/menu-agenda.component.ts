import { Component, OnInit } from '@angular/core';
import { AgendaService } from 'src/app/core/services/http/agenda.service';

@Component({
  selector: 'app-menu-agenda',
  templateUrl: './menu-agenda.component.html',
  styleUrls: ['./menu-agenda.component.css']
})
export class MenuAgendaComponent implements OnInit {

  constructor(private _agendaService:AgendaService) { }

  listaAgenda:any = [];

  ngOnInit(): void {
    this.obtenerAgenda();
  }

  obtenerAgenda(){
    return this._agendaService.listarAgenda()
      .subscribe(res => {
        this.listaAgenda = res;
      })
  }

  hacerChecking(){
    this._agendaService.checking();
  }

  hacerPago(){
    this._agendaService.pago();
  }

}
