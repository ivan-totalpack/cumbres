import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ValidaRutService } from "src/app/core/services/mix/valida-rut.service";
import { LoginService } from  "src/app/core/services/http/login.service"

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private validaRutService: ValidaRutService,
    private _loginService:LoginService
  ) { }

  rut = new FormControl('', [Validators.required]);
  public validatedRut: boolean = false;
  submit:boolean = false;

  ngOnInit(): void {
  }

  tecleado(teclas:string){
    let valida = this.validaRutService.formatearRut(teclas);
    if(valida.length <= 12){
      this.rut.patchValue(valida)
      this.validatedRut = this.validateRut();
    } 
  }

  formatRut() {
    this.validatedRut = this.validateRut();
    this.rut.patchValue(this.validaRutService.formatearRut(this.rut.value));
  }

  private validateRut() {
    const userRut = this.limpiarRut();
    return this.validaRutService.validarRut(userRut);
  }

  private limpiarRut() {
    return this.rut.value.replace(/\./g, "").replace("-", "");
  }

  login(){
    //const rut = this.rut.value.replace(/\./g, "");
    this.submit = true;

    if (this.rut.valid &&  this.validateRut()) {
        this._loginService.login();
    }
  }

}
