import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-checking',
  templateUrl: './menu-checking.component.html',
  styleUrls: ['./menu-checking.component.css']
})
export class MenuCheckingComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
    this.redirecthome();
  }

  redirecthome(){
    setTimeout(() => {
      this.router.navigate(['/']);
    }, 10000);
  }

}
