import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/app/core/services/http/menu.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private _menuService:MenuService) { }

  menu:any = [];

  ngOnInit(): void {
    this.obtenerMenu();
  }

  obtenerMenu(){
    this._menuService.listarMenu()
      .subscribe(res =>{
        this.menu = res;
      })
  }

}
