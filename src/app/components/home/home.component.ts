import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/core/services/http/home.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor( private _homeService:HomeService ) { }

  ngOnInit(): void {
  }

  abrirSeccion(){
    this._homeService.obtenerSeccion();
  }

}
