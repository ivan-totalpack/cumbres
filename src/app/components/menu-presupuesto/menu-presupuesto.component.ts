import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { FormatnumbPipe } from 'src/app/core/pipes/formatnumb.pipe'
import { PresupuestoService } from 'src/app/core/services/http/presupuesto.service'

declare var window: any;

@Component({
  selector: 'app-menu-presupuesto',
  templateUrl: './menu-presupuesto.component.html',
  styleUrls: ['./menu-presupuesto.component.css']
})
export class MenuPresupuestoComponent implements OnInit {

  constructor(private _presupuestoService:PresupuestoService) { }

  listPresupuestos:any = [];
  modal: any;
  radioTotal:boolean = false;
  radioAbonar:boolean = false;
  abono = new FormControl('', [Validators.required]);
  total!:number;

  ngOnInit(): void {
    this.obtenerPresupuestos();

    this.modal = new window.bootstrap.Modal(
      document.getElementById('exampleModal')
    );
  }

  obtenerPresupuestos(){
    this._presupuestoService.listarPresupuestos()
    .subscribe(res => {
      this.listPresupuestos = res;
    })
  }

  openModal(presupuesto:number, iva:number) {
    this.total = presupuesto + iva;
    this.modal.show();
  }

  checkRadio(event:any){
    if(event.target.value == "abonar"){
      this.radioAbonar = true;
      this.radioTotal = false;
    }else if (event.target.value == "total"){
      this.radioTotal = true;
      this.radioAbonar = false;
    }   
  }

  hacerPagao(){
    this.modal.hide();
    this._presupuestoService.pagar()
  }

}
