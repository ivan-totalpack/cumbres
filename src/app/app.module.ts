import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { KeyboardComponent } from './core/utils/keyboard/keyboard.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { MenuComponent } from './components/menu/menu.component';
import { MenuAgendaComponent } from './components/menu-agenda/menu-agenda.component';
import { MenuPresupuestoComponent } from './components/menu-presupuesto/menu-presupuesto.component';
import { MenuExamenesComponent } from './components/menu-examenes/menu-examenes.component';
import { MenuCheckingComponent } from './components/menu-checking/menu-checking.component';

import { PagoDetalleComponent } from './components/pago-detalle/pago-detalle.component';
import { PagarComponent } from './components/pagar/pagar.component';
import { PagoVoucherComponent } from './components/pago-voucher/pago-voucher.component';
import { FormatnumbPipe } from './core/pipes/formatnumb.pipe';



@NgModule({
  declarations: [
    KeyboardComponent,
    HeaderComponent,
    FooterComponent,
    AppComponent,
    LoginComponent,
    HomeComponent,
    MenuComponent,
    MenuAgendaComponent,
    MenuPresupuestoComponent,
    MenuExamenesComponent,
    MenuCheckingComponent,
    PagoDetalleComponent,
    PagarComponent,
    PagoVoucherComponent,
    FormatnumbPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
