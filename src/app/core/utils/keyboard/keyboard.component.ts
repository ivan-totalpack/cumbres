import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { rut } from './teclados';

@Component({
  selector: 'app-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.css']
})
export class KeyboardComponent implements OnInit {

  constructor() { }

  @Output() tecleado = new EventEmitter<string>();
  teclas: any[] = [];
  add_teclas:string = "";

  ngOnInit(): void {
    this.teclado();
  }

  teclado(){
    this.teclas = rut['teclas'];
  }


  selecTecla(accion:string, text:string){
    if(text == 'delete') {
      const delet_tecla = this.add_teclas.substring(0, this.add_teclas.length - 1);
      this.add_teclas = delet_tecla;
      this.tecleado.emit(this.add_teclas);
    }else{
      if(this.add_teclas.length >= 9){
        this.add_teclas == this.add_teclas;
        return
      }
      this.add_teclas += text;
      this.tecleado.emit(this.add_teclas);
    }
  }

}
