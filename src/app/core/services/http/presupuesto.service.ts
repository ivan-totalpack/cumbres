import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { map,catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PresupuestoService {

  constructor(
    private http: HttpClient,
    private router:Router,
    ) { }

    listarPresupuestos(){
      return this.http.get('./assets/api/presupuesto.json')
        .pipe(map((res:any) =>{
          return res.presupuestos;
        }));
    }

    pagar(){
      this.router.navigate(['/pagar']);
     }
}
