import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { map,catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(
    private http: HttpClient,
    private router:Router,
    ) { }

    listarMenu(){
      return this.http.get('./assets/api/menu.json')
        .pipe(map((res:any) =>{
          return res.menu;
        }));
    }
}
