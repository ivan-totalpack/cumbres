import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { map,catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AgendaService {

  constructor(
    private http: HttpClient,
    private router:Router,
    ) { }

    listarAgenda(){
      return this.http.get('./assets/api/agenda.json')
        .pipe(map((res:any) =>{
          return res.agenda;
        }));
    }

    checking(){
      this.router.navigate(['/checking']);
    }

    pago(){
     this.router.navigate(['/detalle-pago']);
    }
}
