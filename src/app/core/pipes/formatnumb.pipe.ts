import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatnumb'
})
export class FormatnumbPipe implements PipeTransform {

  transform(value: any): unknown {;
    return  value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

}
