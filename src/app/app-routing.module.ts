import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { MenuComponent } from './components/menu/menu.component';
import { MenuAgendaComponent } from './components/menu-agenda/menu-agenda.component';
import { MenuPresupuestoComponent } from './components/menu-presupuesto/menu-presupuesto.component';
import { MenuExamenesComponent } from './components/menu-examenes/menu-examenes.component';
import { MenuCheckingComponent } from './components/menu-checking/menu-checking.component';
import { PagoDetalleComponent } from './components/pago-detalle/pago-detalle.component';
import { PagarComponent } from './components/pagar/pagar.component';
import { PagoVoucherComponent } from './components/pago-voucher/pago-voucher.component';

const routes: Routes = [

  { path:'', component: HomeComponent },
  { path:'login', component: LoginComponent },
  { path:'menu', component: MenuComponent },
  { path:'agenda', component: MenuAgendaComponent },
  { path:'presupuesto', component: MenuPresupuestoComponent },
  { path:'examenes', component: MenuExamenesComponent },
  { path:'checking', component: MenuCheckingComponent },
  { path:'detalle-pago', component: PagoDetalleComponent },
  { path:'pagar', component: PagarComponent },
  { path:'pago-voucher', component: PagoVoucherComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
